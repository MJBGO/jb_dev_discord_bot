import * as Discord from 'discord.js';
import * as logger from 'winston';
import * as moment from 'moment';
import * as mysql from 'mysql';

// Create an instance of a Discord client
const client = new Discord.Client();

const STATUS_OFFLINE = 'offline';
const DB_DATE_FORMAT = 'YYYY/MM/DD H:mm:ss';
const CHANNEL_CREATE_CHANNEL = '664173335227269152';
const CHANNEL_GAME_CATEGORY = '664115253616967701';
const CHANNEL_NAMES = ['alfa', 'bravo', 'charlie', 'delta', 'echo', 'foxtrot', 'golf', 'hotel', 'india', 'juliett', 'kilo', 'lima', 'mike', 
'november', 'oscar', 'papa', 'quebec', 'romeo', 'sierra', 'tango', 'uniform', 'victor', 'whiskey', 'x-ray', 'yankee', 'zoulou'];

// Configure logger settings
logger.configure({
    level: 'info',
    transports: [
        new logger.transports.Console()
    ]
});

client.on('ready', () => {
    if (client.user) {
        client.user.setActivity('--help', {type: 'LISTENING'});
    } else {
        logger.error("Cannot set activity because client is null");
    }
});

client.on('message', (message: Discord.Message) => {
    // Auto delete message
    if (message.content.startsWith('>')) {
        message.delete({timeout: 600000});
    }

    // Add GIF
    if (!message.author.bot) {
        logger.info(`Message reçu de ${message.author.username}: ${message.content}`);
        const msg = message.content.toUpperCase();
        const msgArray = msg.split(' ');

        if (msgArray.includes('PING')) {
            message.channel.send('pong');
        } else if (msgArray.includes('RIP')) {
            const attachment = new Discord.MessageAttachment('https://media.giphy.com/media/aBEh0DBRK7yzS/giphy.gif');
            message.channel.send(attachment);
        } else if (msgArray.includes('PD')) {
            const attachment = new Discord.MessageAttachment('https://media.giphy.com/media/LeYFkniJST8wo/giphy.gif');
            message.channel.send(attachment);
        } else if (msgArray.includes('TAGGLE')) {
            const attachment = new Discord.MessageAttachment('https://media.giphy.com/media/M8xmO5ZcLPtAY/giphy.gif');
            message.channel.send(attachment);
        } else if (msgArray.includes('BISOUS')) {
            const attachment = new Discord.MessageAttachment('https://media.giphy.com/media/dMYVHzANYb9p6/giphy.gif');
            message.channel.send(attachment);
        } else if (msgArray.includes('GG')) {
            const attachment = new Discord.MessageAttachment('https://media.giphy.com/media/ZdO4NenDbsQNwUwKDB/giphy.gif');
            message.channel.send(attachment);
        } else if (msgArray.includes('BG')) {
            const attachment = new Discord.MessageAttachment('https://media.giphy.com/media/3ohjV4VbIcx65zsBYQ/giphy.gif');
            message.channel.send(attachment);
        } else if (msgArray.includes('BEER')) {
            const attachment = new Discord.MessageAttachment('https://media.giphy.com/media/mOOuUUIEEgq6A/giphy.gif');
            message.channel.send(attachment);
        } else if (msg === '--JOIN') {
            if (message.member && message.member.voice && message.member.voice.channel) {
                message.member.voice.channel.join();
            }
        } else if (msg === '--LEAVE') {
            if (message.member && message.member.voice && message.member.voice.channel) {
                message.member.voice.channel.leave();
            }
        } else if (msg === '--HELP') {
            message.channel.send({embed: {
                color: 1560111,
                fields: [{
                    name: 'ping',
                    value: 'Writes "pong"'
                }, {
                    name: 'rip',
                    value: 'Sends a nice image'
                }, {
                    name: 'pd',
                    value: 'Sends a nice image'
                }, {
                    name: 'taggle',
                    value: 'Sends a nice image'
                }, {
                    name: 'bisous',
                    value: 'Sends a nice image'
                }, {
                    name: 'gg',
                    value: 'Sends a nice image'
                }, {
                    name: 'bg',
                    value: 'Sends a nice image'
                }, {
                    name: 'beer',
                    value: 'Sends a nice image'
                }, {
                    name: '--join',
                    value: 'Adds the bot to a voice channel'
                }, {
                    name: '--leave',
                    value: 'Removes the bot from a voice channel'
                }, {
                    name: '--help',
                    value: 'Lists all commands when you need help'
                }],
            }});
        }
    }
});

client.on('presenceUpdate', (oldPresence: Discord.Presence | undefined, newPresence: Discord.Presence | undefined) => {
    if (newPresence && newPresence.user) {
        if (oldPresence && oldPresence.status === newPresence.status) {
            if (newPresence.activities.length > 0) {
                sendPresenceUpdate(new Date(), 'MEMBER_START_PLAYING', newPresence.user.username, newPresence.activities[0].name);
            } else if (oldPresence.activities.length > 0) {
                sendPresenceUpdate(new Date(), 'MEMBER_STOPPED_PLAYING', newPresence.user.username, oldPresence.activities[0].name);
            }
        } else {
            if (newPresence.status === STATUS_OFFLINE) {
                sendPresenceUpdate(new Date(), 'MEMBER_OFFLINE', newPresence.user.username);
            } else if (!oldPresence || oldPresence.status === STATUS_OFFLINE) {
                sendPresenceUpdate(new Date(), 'MEMBER_ONLINE', newPresence.user.username);
            }
        }
    }
});


client.on('voiceStateUpdate', (oldState: Discord.VoiceState, newState: Discord.VoiceState) => {
    let channel;
    if (oldState.channelID) {
        channel = newState.guild.channels.cache.get(oldState.channelID) as Discord.VoiceChannel;
    }

    if (channel != null && channel.members.size == 0 && channel.parentID === CHANNEL_GAME_CATEGORY && channel.id !== CHANNEL_CREATE_CHANNEL) {
        channel.delete();
    }

    if (newState.channelID === CHANNEL_CREATE_CHANNEL) {
        let channelName = '';
        do {
            channelName = 'Salon ' + capitalizeFirstLetter(CHANNEL_NAMES[Math.floor(Math.random() * 26)]);
        } while (newState.guild.channels.cache.find(channel => channel.name === channelName) !== undefined);

        newState.guild.channels.create(channelName, {type: 'voice', parent: CHANNEL_GAME_CATEGORY}).then(channel => {
            if (newState.member) {
                newState.member.voice.setChannel(channel);
            }
        });
    }
});

function sendPresenceUpdate(createdAt: Date, status: String, username: String, data?: String): void {
    const mysqlClient = mysql.createConnection({
        host: process.env.DISCORD_BOT_DB_HOST,
        user: process.env.DISCORD_BOT_DB_USER,
        password: process.env.DISCORD_BOT_DB_PASSWORD,
        database: process.env.DISCORD_BOT_DB_NAME
    });

    logger.info(`Changed status of ${username} : ${status} / ${data}`);

    const queryData = [[username, status, data, moment(createdAt).format(DB_DATE_FORMAT)]];
    mysqlClient.query("INSERT INTO presence_updates (username, status, data, created_at) VALUES (?)", queryData, function (error, results, fields) {
        if (error) {
            logger.error('An error has occurred : ' + error.sql)
        }
    });
}

function capitalizeFirstLetter(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

client.login("NTY5MTM5MjU3NDk1NTE5MjQy.XUChiQ.ovE1Z-PRK0jtZEuYqacQgDqxRF0")