module.exports = {
    apps : [
        {
            name: "JB Dev Discord bot",
            env_production: {
                NODE_ENV: "production"
            },
            script: "npm",
            args: "start"
        }
    ]
}